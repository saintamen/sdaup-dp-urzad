package com.amen.somedepartment;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

public class Department {
	List<Office> listaPokoi;

	public Department() {
		listaPokoi = new ArrayList<>();
	}
	
	public void addOffice(Office o){
		listaPokoi.add(o);
	}
	
	public Office getOffice(int indeksBiura){
		return listaPokoi.get(indeksBiura);
	}
}
